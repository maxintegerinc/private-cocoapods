Pod::Spec.new do |s|

  s.name         = "TestFramework"
  s.version      = "1.0.0"
  s.summary      = "A sample framework for testing private cocoapods."

  s.homepage     = "http://apple.com"
  s.license      = "Private"
  s.author       = { "Chris Miller" => "chrismiller@msn.com" }
  s.platform     = :ios, "8.0"

  s.source       = { :git => "https://bitbucket.org/maxintegerinc/TestFramework.git", :tag => s.version }

  s.source_files  = "TestFramework/**/*.{swift}"

end
